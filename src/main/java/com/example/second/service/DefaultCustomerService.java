package com.example.second.service;

import com.example.second.dao.AccountDao;
import com.example.second.dao.CustomerDao;
import com.example.second.domain.Account;
import com.example.second.domain.Currency;
import com.example.second.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class DefaultCustomerService implements CustomerService{
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private AccountDao accountDao;
    @Override
    public List<Customer> getAll() {return customerDao.findAll();
    }
    @Override
    public Customer getById(Long customerId) {
        return customerDao.getOne(customerId);
    }
    @Override
    public boolean update(Customer customer) {
//        Customer notUpdatedCustomer = customerDao.getOne(customer.getId());
//        if (notUpdatedCustomer != null) {
//            customerDao.delete(notUpdatedCustomer);
            customerDao.save(customer);
            return true;
    }
//        return false;
//}
    @Override
    public void delete(long id) {
        customerDao.getOne(id).getAccounts().forEach(e -> accountDao.delete(e));
        customerDao.delete(customerDao.getOne(id));
    }
    @Override
    public void create(Customer customer) {
        customerDao.save(customer);
    }
    @Override
    public void createCustomerAccount(Long customerId, Currency currency) {
        Customer customer = customerDao.getOne(customerId);
        Account account = new Account(currency, customer);
        List<Account> customerAccounts = customer.getAccounts();
        customerAccounts.add(account);
        customer.setAccounts(customerAccounts);
        accountDao.save(account);}
    @Override
    public boolean deleteCustomerAccount(Long customerId, Long accountId) {
        Account deleteAccount = customerDao.getOne(customerId).getAccounts().stream().filter(e -> e.getId().equals(accountId)).findFirst().get();
        if(deleteAccount!=null){
            accountDao.delete(deleteAccount);
              return  customerDao.getOne(customerId).getAccounts().remove(deleteAccount);

            };
        return false;

        }
}

















