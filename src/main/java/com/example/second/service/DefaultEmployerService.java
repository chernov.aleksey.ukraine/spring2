package com.example.second.service;

import com.example.second.dao.CustomerDao;
import com.example.second.dao.EmployerDao;
import com.example.second.domain.Employer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultEmployerService implements EmployerService{
    @Autowired
    private EmployerDao employerDao;
    @Autowired
    private CustomerDao customerDao;


    @Override
    public Employer getById(Long id) {
        return employerDao.getOne(id);
    }

    @Override
    public List<Employer> getAllEmployers() {
        return employerDao.findAll();
    }

    @Override
    public Employer createEmployer(Employer newEmployer) {
        return employerDao.save(newEmployer);
    }

    @Override
    public Employer createEmployer(String name, String address) {
        return employerDao.save(new Employer(name, address));
    }

    @Override
    public Employer updateEmployer(Employer employer) {
        if(employer.getId() == null){
            return null;
        }
        return employerDao.save(employer);

    }

    @Override
    public Employer updateEmployer(Long id, String name, String address) {
        if(id == null){
            return null;
        }
        return employerDao.save(new Employer(id, name, address));

    }

    @Override
    public boolean deleteEmployer(Employer employer) {
        return employerDao.delete(employer);
    }

    @Override
    public boolean deleteEmployerById(Long id) {

        customerDao.deleteEmployerForAll(this.getById(id));

        return this.deleteEmployer(this.getById(id));
    }
}
