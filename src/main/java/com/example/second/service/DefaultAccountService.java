package com.example.second.service;

import com.example.second.dao.AccountDao;
import com.example.second.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class DefaultAccountService implements AccountService{
    @Autowired
    private AccountDao accountDao;
    @Override
    public List<Account> getAll() {
        return accountDao.findAll();
    }
    @Override
    public Account getById(Long userId) {
        return accountDao.getOne(userId);
    }
    @Override
    public void delete(long id) {
        accountDao.delete(accountDao.getOne(id));
    }
    @Override
    public void create(Account account) {
        accountDao.save(account);
    }
    @Override
    public void topUpAccount(String accountNumber, Double amount) {
        accountDao.getByAccountNumber(accountNumber).topUp(amount);
    }
    @Override
    public boolean withdrawMoney(String accountNumber, Double amount) {
        return accountDao.getByAccountNumber(accountNumber).withdrawMoney(amount);
    }
    @Override
    public boolean transferMoney(String numberAccountFrom, Double amount, String numberAccountTo) {
        Account accountFrom = accountDao.getByAccountNumber(numberAccountFrom);
        Account accountTo = accountDao.getByAccountNumber(numberAccountTo);
        return accountFrom.transferMoney(amount, accountTo);}
}
