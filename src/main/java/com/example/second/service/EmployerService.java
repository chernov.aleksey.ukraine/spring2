package com.example.second.service;

import com.example.second.domain.Employer;

import java.util.List;

public interface EmployerService {
    Employer getById(Long id);
    List<Employer> getAllEmployers();
    Employer createEmployer(Employer newEmployer);
    Employer createEmployer(String name, String address);
    Employer updateEmployer(Employer employer);
    Employer updateEmployer(Long id, String name, String address);
    boolean deleteEmployer(Employer employer);
    boolean deleteEmployerById(Long id);

}
