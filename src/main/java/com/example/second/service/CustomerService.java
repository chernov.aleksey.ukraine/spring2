package com.example.second.service;

import com.example.second.domain.Account;
import com.example.second.domain.Currency;
import com.example.second.domain.Customer;
import java.util.List;
public interface CustomerService {
    List<Customer> getAll();
    Customer getById(Long userId);
    void delete(long id );
    boolean update(Customer customer);
    void create(Customer employee);
    void createCustomerAccount (Long customerId, Currency currency);
    boolean deleteCustomerAccount (Long customerId, Long accountId);
}
