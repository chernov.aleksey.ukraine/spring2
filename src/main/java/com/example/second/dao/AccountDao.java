package com.example.second.dao;

import com.example.second.domain.Account;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceUnit;
import jakarta.persistence.Query;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class AccountDao implements Dao<Account>{
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;


    @Override
    public Account save(Account account) {
        Account existAccountById = null;
        Account result = null;

        if(account.getId() != null){
            existAccountById = this.getOne(account.getId());
        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            if(existAccountById == null){
                entityManager.persist(account);

                List<Account> list = this.findAll().stream()
                        .filter(el -> el.getNumber() == account.getNumber()
                                && el.getCurrency() == account.getCurrency())
                        .toList();
                if(!list.isEmpty()) {
                    result = list.get(0);
                }
            } else {
                entityManager.merge(account);
                result = this.getOne(account.getId());
            }
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Saving the account " + account + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return result;

    }

    @Override
    public boolean delete(Account account) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            account = entityManager.find(Account.class, account.getId());
            entityManager.getTransaction().begin();
            entityManager.remove(account);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the account " + account + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;

    }

    @Override
    public void deleteAll(List<Account> entities) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entities.forEach(el -> entityManager.remove(el));
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the list of accounts " + entities + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void saveAll(List<Account> entities) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entities.forEach(el -> {
                if(el.getId() != null && this.getOne(el.getId())!= null) {
                    entityManager.merge(el);
                } else {
                    entityManager.persist(el);
                }
            });
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Saving the list of accounts " + entities + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public List<Account> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("from Account e ");
        List<Account> resultList = query.getResultList();
        return resultList;
    }

    @Override
    public boolean deleteById(long id) {
        Account account = this.getOne(id);
        if(account == null){
            return false;
        }
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(account);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the account " + account + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;
    }

    @Override
    public Account getOne(long id) {
        Account account = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            account = entityManager.find(Account.class, id);
        } catch (HibernateException ex){
            log.error("Account with id = " + id + " not found");
        } finally {
            entityManager.close();
        }
        return account;
    }

    public Account getByAccountNumber(String accountNumber) {
        Account account = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager
                .createQuery("select e from Account e where e.number = :number")
                .setParameter("number", accountNumber);
        try {
            account = (Account) query.getSingleResult();
        } catch (HibernateException ex){
            log.error("Account with number = " + accountNumber + " not found");
        } finally {
            entityManager.close();
        }
        return account;

    }
}
