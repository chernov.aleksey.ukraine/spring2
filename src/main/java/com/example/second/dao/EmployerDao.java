package com.example.second.dao;

import com.example.second.domain.Employer;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.PersistenceUnit;
import jakarta.persistence.Query;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class EmployerDao implements Dao<Employer>{
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public Employer save(Employer employer) {
        Employer existEmployerById = null;
        Employer result = null;

        if(employer.getId() != null){
            existEmployerById = this.getOne(employer.getId());
        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            if(existEmployerById == null){
                entityManager.persist(employer);
//                result = this.findAll().stream()
//                        .filter(el -> el.getName() == employer.getName()
//                                && el.getAddress() == employer.getAddress())
//                        .toList().get(0);
                result = employer;
            } else {
                entityManager.merge(employer);
                result = this.getOne(employer.getId());
            }
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Saving the customer " + employer + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public boolean delete(Employer employer) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            employer = entityManager.merge(employer);

            entityManager.remove(employer);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the employer " + employer + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;

    }

    @Override
    public void deleteAll(List<Employer> employers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            employers.forEach(el -> entityManager.remove(el));
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the list of employers " + employers + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public void saveAll(List<Employer> employers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            employers.forEach(el -> {
                if(el.getId() != null && this.getOne(el.getId())!= null) {
                    entityManager.merge(el);
                } else {
                    entityManager.persist(el);
                }
            });
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Saving the list of employers " + employers + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public List<Employer> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("from Employer e ");
        List<Employer> resultList = query.getResultList();
        return resultList;

    }

    @Override
    public boolean deleteById(long id) {
        Employer employer = this.getOne(id);
        if(employer == null){
            return false;
        }
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(employer);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the employer " + employer + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;

    }

    @Override
    public Employer getOne(long id) {
        Employer employer = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            employer = entityManager.find(Employer.class, id);
        } catch (HibernateException ex){
            log.error("Employer with id = " + id + " not found");
        } finally {
            entityManager.close();
        }
        return employer;

    }
}
