package com.example.second.dao;

import com.example.second.domain.Account;
import com.example.second.domain.Customer;
import com.example.second.domain.Employer;
import jakarta.persistence.*;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
@Slf4j
public class CustomerDao implements Dao<Customer>{
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public Customer save(Customer customer) {
        Customer existCustomerById = null;
        Customer result = null;

        if(customer.getId() != null){
            existCustomerById = this.getOne(customer.getId());
        }

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            if(existCustomerById == null){
                entityManager.persist(customer);

                List<Customer> list = this.findAll().stream()
                        .filter(el -> el.getName() == customer.getName()
                                && el.getEmail() == customer.getEmail()
                                && el.getAge() == customer.getAge())
                        .toList();
                if(!list.isEmpty()) {
                    result = list.get(0);
                }

            } else {
                entityManager.merge(customer);
                result = this.getOne(customer.getId());
            }
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Saving the customer " + customer + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
        return result;

    }

    @Override
    public boolean delete(Customer customer) {
        this.deleteById(customer.getId());
        return true;
    }

    @Override
    public void deleteAll(List<Customer> customers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            customers.forEach(el -> entityManager.remove(el));
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the list of customers " + customers + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }

    }
    public void deleteEmployerForAll(Employer employer) {
        System.out.println(employer.getId());
        List<Customer> customers = this.findAll();
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();

            customers.forEach(el -> {
                System.out.println(el.getId());
                System.out.println(el.getEmployers());
//                if (el.getEmployers().contains(employer)){
//                    System.out.println("contains");
//                    el.getEmployers().remove(employer);
//                    entityManager.merge(el);
//                }
                List<Employer> employerList = new ArrayList<>();
                if (el.getEmployers().size()>0){
                el.getEmployers().forEach(entry -> {if (!entry.getId().equals(employer.getId())){employerList.add(entry);}});
                el.setEmployers(employerList);
                    entityManager.merge(el);
                }
            });

            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Correcting the list of customer's Employers " + customers + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }

    }


    @Override
    public void saveAll(List<Customer> customers) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            customers.forEach(el -> {
                if(el.getId() != null && this.getOne(el.getId())!= null) {
                    entityManager.merge(el);
                } else {
                    entityManager.persist(el);
                }
            });
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Saving the list of customers " + customers + " failed");
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }

    }

    @Override
    public List<Customer> findAll() {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Query query = entityManager.createQuery("from Customer e ");
        List<Customer> resultList = query.getResultList();
        return resultList;

//        List<Customer> customers = null;
//        EntityManager entityManager = entityManagerFactory.createEntityManager();
//        EntityGraph entityGraph = entityManager.createEntityGraph("groupWithStudents");
//        try{
//            customers = entityManager.createQuery("FROM Customer", Customer.class)
//                    .setHint("jakarta.persistence.fetchgraph", entityGraph)
//                    .getResultList();
//        } catch (HibernateException ex) {
//            log.error("Customer not found");
//        } finally {
//            entityManager.close();
//        }
//        return customers;
    }

    @Override
    public boolean deleteById(long id) {
        Customer customer = this.getOne(id);
        if(customer == null){
            return false;
        }
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            customer = entityManager.merge(customer);
            entityManager.remove(customer);
            entityManager.getTransaction().commit();
        } catch (HibernateException ex){
            log.error("Deleting the customer " + customer + " failed");
            entityManager.getTransaction().rollback();
            return false;
        } finally {
            entityManager.close();
        }
        return true;

    }

    @Override
    public Customer getOne(long id) {
        System.out.println(id);
        Customer customer = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            Query query = entityManager.createQuery("Select c from Customer c where c.id = :id")
                    .setParameter("id", id);
            customer = (Customer) query.getSingleResult();
            System.out.println(customer);
        } catch (HibernateException ex){
            log.error("Customer with id = " + id + " not found");
        } finally {
            entityManager.close();
        }
        return customer;
    }
}
