package com.example.second.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Entity
@Table(name = "accounts")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"customer"})
@EqualsAndHashCode(of = "id")
public class Account extends AbstractEntity {

    private final String number = UUID.randomUUID().toString();
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private Double balance = 0D;
    @JsonIgnore
    @ManyToOne
    private Customer customer;


    public Account(Currency currency, Customer customer) {
        this.currency = currency;
        this.customer = customer;
    }

    public void topUp(Double amount) {
        setBalance(balance + amount);
    }

    public boolean withdrawMoney(Double amount) {
        if(balance >= amount) {
            setBalance(balance - amount);
            return true;
        }
        return false;
    }

    public boolean transferMoney(Double amount, Account toAccount) {
        if(balance >= amount) {
            setBalance(balance - amount);
            toAccount.setBalance(toAccount.getBalance() + amount);
            return true;
        }
        return false;
    }

}
