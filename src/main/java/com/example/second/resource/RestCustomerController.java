package com.example.second.resource;

import com.example.second.domain.Account;
import com.example.second.domain.Currency;
import com.example.second.domain.Customer;
import com.example.second.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequiredArgsConstructor
@RequestMapping("/customers")
//@Slf4j
@CrossOrigin(origins = {"http://localhost:3000"})
public class RestCustomerController {
    @Autowired
    private final CustomerService customerService;

    public RestCustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public List<Customer> getAll() {

        return customerService.getAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") String customerId) {
        try {
            return ResponseEntity.ok(customerService.getById(Long.parseLong(customerId)));
        } catch (RuntimeException e) {
//            log.error("Customer not found", e);
            return ResponseEntity.badRequest().body("Customer not found");
        }
    }

    @PostMapping
    public void createCustomer(String name, String email, Integer age){
        Customer customer = new Customer(name, email, age);
        customerService.create(customer);
    }

    @PutMapping()
    public void updateCustomer(@RequestBody Customer customer) {
        customerService.update(customer);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity <?> deleteById(@PathVariable("id") String customerId) {
        try {
            customerService.delete(Long.parseLong(customerId));
            return ResponseEntity.ok().build();
        } catch (RuntimeException e){
//            log.error("Customer not found", e);
            return ResponseEntity.badRequest().body("Customer not found");
        }
    }

    @PutMapping("/create-account")
    public void createCustomerAccount(Long customerId, Currency currency) {
        customerService.createCustomerAccount(customerId, currency);
    }

    @PutMapping("/delete-account")
    public boolean deleteCustomerAccount(Long customerId, Long accountId) {
       return customerService.deleteCustomerAccount(customerId, accountId);
    }

}
