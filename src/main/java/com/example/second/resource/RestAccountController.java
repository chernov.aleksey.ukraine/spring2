package com.example.second.resource;

import com.example.second.domain.Account;
import com.example.second.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
//@RequiredArgsConstructor
@RequestMapping("/accounts")
//@Slf4j
@CrossOrigin(origins = {"http://localhost:3000"})
public class RestAccountController {
    @Autowired
    private final AccountService accountService;

    public RestAccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public List<Account> getAll() {return accountService.getAll();}
//    Поповнити рахунок (приймає номер рахунку та суму)
    @PutMapping("/top-up")
    public void topUpAccount(String accountNumber, Double amount) {
        accountService.topUpAccount( accountNumber , amount);}
//    зняти гроші з рахунку (приймає номер рахунку та суму, виконується тільки якщо на рахунку достатньо грошей)
    @PutMapping("/withdraw")
    public boolean withdrawMoney(String accountNumber, Double amount) {
        if (accountService.withdrawMoney( accountNumber, amount)) {return true;}
//        log.error("Error");
        return false;}
//    переказати гроші на інший рахунок (приймає два номери рахунку та суму, виконується тільки якщо на рахунку достатньо грошей)
    @PutMapping("/transfer")
    public boolean transferMoney(String numberAccountFrom, Double amount, String numberAccountTo) {
        if (accountService.transferMoney(numberAccountFrom, amount, numberAccountTo)) {return true;}
//        log.error("Error");
        return false;
    }
}
