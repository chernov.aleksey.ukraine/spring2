package com.example.second.resource;

import com.example.second.domain.Employer;
import com.example.second.service.EmployerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"http://localhost:3000"})
@RequestMapping("/employers")
@RequiredArgsConstructor
public class RestEmployerController {
    @Autowired
    private EmployerService employerService;

    @GetMapping("/")
    public ResponseEntity<?> getAllEmployers(){
        return ResponseEntity.ok(employerService.getAllEmployers());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        Employer employer = employerService.getById(id);
        return employer != null
                ? ResponseEntity.ok().body(employer)
                : ResponseEntity.badRequest().body("Employer with id " + id + " not found");
    }
    @PostMapping("/")
    public ResponseEntity<?> postNewUser(String name, String adr) {
        Employer newEmployer = new Employer(name, adr);
                employerService.createEmployer(newEmployer);
        return newEmployer != null
                ? ResponseEntity.ok().body(newEmployer)
                : ResponseEntity.badRequest().body("Saving error");
    }
    @PutMapping("/")
    public ResponseEntity<?> updateUser(@RequestBody Employer employer){
        Employer newEmployer = employerService.updateEmployer(employer);
        return newEmployer != null
                ? ResponseEntity.ok().body(newEmployer)
                : ResponseEntity.badRequest().body("Updating error");
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id){
        return employerService.deleteEmployerById(id)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Employer not found");
    }

}
